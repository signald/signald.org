#!/usr/bin/env python
import anybadge
import requests

BLUE = "#007ec6"

def pretty(n, label=None):
    suffix = "" if label is None else f" {label}"
    if n > 1000000000:
        value = n/1000000000
        return "{:.1f}B{}".format(value, suffix)
    if n > 1000000:
        value = n/1000000
        return "{:.1f}M{}".format(value, suffix)
    if n > 1000:
        value = n/1000
        return "{:.1f}k{}".format(value, suffix)
    return f"{n}{suffix}"

# docker badge
docker = requests.get("https://hub.docker.com/v2/repositories/finn/signald/").json()
label = pretty(docker.get('pull_count'), "pulls")
anybadge.Badge('docker', label, default_color=BLUE).write_badge('static/badges/docker.svg')

# gitlab badge
gitlab = requests.get("https://gitlab.com/api/v4/projects/7028347").json()
label = pretty(gitlab.get('star_count'), "stars")
anybadge.Badge('gitlab', label, default_color=BLUE).write_badge('static/badges/stars.svg')
