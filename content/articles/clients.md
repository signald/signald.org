---
title: Clients
---

A list of signald clients. [Submit a merge request to add to this list](https://gitlab.com/signald/signald.org/-/blob/main/content/articles/clients.md)

* [signaldctl](https://signald.org/signaldctl/) - simple cli for account creation and other maintenance functions. No receive functionality.
* [libpurple-signald](https://github.com/hoehermann/libpurple-signald)
* [matrix-signal](https://github.com/tulir/mautrix-signal)
* [signal-weechat](https://github.com/thefinn93/signal-weechat)
* [Adhesive](https://github.com/signalstickers/Adhesive) - A chatbot serving as your glue between Telegram and Signal sticker packs
* [alertmanager-webhook-signald](https://github.com/dgl/alertmanager-webhook-signald) -  Alertmanager webhook server for Signald
* [spectrum2_signald](https://gitlab.com/nicocool84/spectrum2_signald/) - An XMPP gateway to signald based on [spectrum](https://spectrum.im/)
* [slidge](https://gitlab.com/nicocool84/slidge/) - An XMPP gateway to signald using the SliXMPP library.
* [signal-latex-bot](https://github.com/inthewaves/signal-latex-bot) - A Signal bot that replies to incoming messages with LaTeX PNGs
* [signald-smtp-bridge](https://github.com/lawrencegripper/signald-smtp-bridge) - An SMTP server which receives emails and sends them via signald
* [signalblast](https://github.com/Era-Dorta/signalblast) - A tool to send encrypted messages anonymously over Signal to a subscriber list
