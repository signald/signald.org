---
title: Install on Debian
---

{{<hint info>}}
This is tested on Debian Stable, but expected to work on newer releases as well. It will also probably work on most Debian derratives such as Ubuntu, Mint, etc.

Does not work on extremely old versions of Debian (such as buster, oldstable at time of writing) or near-EOL versions of Ubuntu. There is a [janky workaround](https://gitlab.com/signald/signald/-/issues/354#note_1248431196) for those if you want to try it.
{{</hint>}}

put the following in `/etc/apt/sources.list.d/signald.list`:

```
deb [signed-by=/usr/share/keyrings/signald.gpg] https://updates.signald.org unstable main
```

And trust the signing key:

```
sudo wget -O /usr/share/keyrings/signald.gpg https://signald.org/signald.gpg
```

Update the package list:

```
sudo apt update
```

{{<hint info>}}
if you get an error message like this:
```
Certificate verification failed: The certificate is NOT trusted. The certificate issuer is unknown.  Could not handshake: Error in the certificate verification.
```

edit `/etc/apt/sources.list.d/signald.list` and switch `https` to `http`. Then run `sudo apt update` again.

The package is still signed by the trusted signing key, so this will not create a significant security issue.

More information in [#312](https://gitlab.com/signald/signald/-/issues/312)
{{</hint>}}

Now you can install signald:

```
sudo apt install signald
```

Ensure the service is running:
```
sudo systemctl start signald
```

verify that the installation was successful:

```
signaldctl version
```

Other useful commands:

* see old logs: `journalctl -u signald`
* see new logs: `journalctl -fu signald` will show new logs as they appear, ctrl+c to stop
* check if signald is running: `systemctl status signald`
* restart signald: `sudo systemctl restart signald`
* allow a non-root user to access signald (replace `exampleuser` with username): `sudo adduser exampleuser signald`
