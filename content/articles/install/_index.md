* on [Debian](/articles/install/debian/)
* with a [Container runtime](/articles/install/container/) (like Docker or Podman)
* from [source](/articles/install/source/)
