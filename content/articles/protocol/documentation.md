---
title: Documentation
aliases:
    - /articles/protocol-documentation/
---

The signald client protocol can document itself in JSON format. The actual data structure of the documentation is not
documented at this time.

There are a few ways to get the protocol documentation from signald:
* `signaldctl protocol`: [man page](/signaldctl/reference/signaldctl_protocol/)
* through the client socket: `{"type": "protocol", "version": "v1"}`
  * the only request type NOT included in the protocol document itself. The response type is the protocol document.
* by starting signald with a special flag: `signald --dump-protocol > protocol.json`

Additionally, the latest version of the protocol document can be downloaded from [signald.org/protocol.json](/protocol.json)

# Examples

some things that are known to use the protocol documentation:

* Parts of this site are generated. See [generate.go](https://gitlab.com/signald/signald.org/-/blob/main/generate.go),
    which creates the markdown files for the protocol data. The final result can be found in the sidebar, under "Protocol"
* signald-go is mostly generated. See [tools/generator/main.go](https://gitlab.com/signald/signald-go/-/blob/main/tools/generator/main.go).
* aiosignald is in large part generated. See [generate.py](https://git.sr.ht/~nicoco/aiosignald/tree/master/item/generate.py).
