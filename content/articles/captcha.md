---
title: Captchas
---

Sometimes Signal requires a completion of a captcha to register. When this happens, signald throws a
`CaptchaRequiredException` error. If that error is thrown the user must complete a captcha.

## Getting A Token

1. Visit https://signalcaptchas.org/registration/generate and complete the captcha
1. The browser will prompt you to launch an external application, which you should decline.
1. An "Open Signal" link will appear below the captcha. Copy the link, can be used as your captcha token.

## Register With A Token

Most users will simply need to provide the captcha token to their client. Client authors should simply
include the captcha token in a registration request.

{{<tabs "register">}}
{{<tab "signaldctl">}}
```
signaldctl account register +12024561414 --captcha signalcaptcha://signal-hcaptcha.5fad97ac-7d06-4e44-b18a-b950b20148ff.registration.P1_eyJ...
```
{{</tab>}}
{{<tab "raw">}}
```json
{
    "type": "register",
    "version": "v1",
    "account": "+12024561414",
    "captcha": "signalcaptcha://signal-hcaptcha.5fad97ac-7d06-4e44-b18a-b950b20148ff.registration.P1_eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.hadwYXNza2V5xQTD9GUmr4A28qAzNvsFTNUvTdCbd8fKWCk8D_u0kvDBMDyIFBeHJueC_BLzDPXruRV2dq1CjGCDlDcslvX0OBxhfqsoieAH9gZ-91lwBEMxIm8tHTyJosInEpufUV394F6ALiiAzhmodlEDgnWtLL2ZA3KT79MVuTomwTL-hxV-MQCUVSplWNknVAWGot0UaVG49hiGogAVjUfb3pqWdz86l_Zqt2LwX7DlDGgBDoegX8cjng4Fq1pG35WYq6fuq4yTdtUPo82Ft19zk0idiYMWn6zI1Etb6S1zOSmIg7EvylZlIrSglEAPAZOxtD5IiXhRJvi2f_Pc7GeOEJ1FvFs2M8kVqN3pXV4r6mAlq6ZCc6BUcmjEzSv68PakA1sRZ3Rf1i3cMLP_-y0kItAVRqcz4YRXX7gO59qix4xKCdihJMKtpi4TGPyWetW8Veu2wbc8rKeGKH-yY5jCU-v8OgHySO0JwTTTkuFZkxu0yaQWVSvHMwIfohb8q-xpO3cY08-S2nrbroekmpWagB5HjROaZrf7yplPDuqi3LbxEikGf_TPeSVuayQSNdWfEJ-69ug6ambP_aXQxjgDRgIIhVp6iHDxPih1CPBVunn7edeuqjTO0eXNfXxetX3O_L0GR8j1BhYGzK6C8FVDQJOMm97AJcurlD6y1IOUqf-4ot70ERAljCJXy4nNjkRRFOLkyDpcvmTNqTnhVt9XvB5DNIztKG5i4bgjnm72m2BBmWXG0qiRA2lxZjZkp8IFAwflrz-uleVF3sMQlhly1KArrH-pyio42aylbwmo4nazHXq_Me6VZvitAgxC6JfH-N98A7Vu60Mv46QcBMST3QtHSNA1WNNjduqUEpH1b0jfefALC2Klc3VUyMruGUJpn5bJG7mpItmW2cGeVBvk2uVoNFvpxEZcVfqg3XNQ3iHqgR7xn_kHeCR8rwxzV0Omiftmc6tc8Kjpq05pG9IZI3Ho70DQoOjv74SDwo7E88lfWj0T83pm0ByMkdhXC97-P54iGgLR8e70ETthUZwLeIxrmf5XlYUf8L7xbQINEmQ8ixdqNqNcyBDzZyRT62cQQDFrsymQyFX3y5VREmYlVi94eFszyxHk6y9tcMpCbNMHY2sCnyJvHfUeRZXjRRvMcYw68FT1oBFMrDRHzhN40n24OYdDnHet6R8rVlarBb5alvGxbcmy5syaLDiBhKZ_GS6e_y_Kn-9Dwfj6W4IQwKS-HRo3pJ3R3IXym48IXuFLAmJBJscS0-z8QLNf4-8Z2gV5ksI3uPLvhuEZe6cakI1teFG7V6U6Jaer_C8JfVDzo_eZmhyCDiUHpDTVOUdMU6w8tgfDn8SJdzAUajhOm9i2fPhZTyXRkK2jEiSqhra8e3iaJzH3b1gs0rQrl8i7DKDNzAMdGgMgB33DrWz7hvXIfOcj23Z3N1zcskgVwFC9SYL6xR-BkWKqboq2cTwRwKCUESDLKL3a8vpW2Vew8Aq71-606Y1OH76Lop5DlnLi_FwljNW3ekjsDjdIT4fbqgF9RMvpmNi1yQVZ4Nunlgw4eiHKzF2ONtJhNy51TrW6gn3PgvLXRUB6SF-x_8GnhkxUpCDTCW9A2SAnkEwuAZiKiRWc5FdceqNleHDOZkqVYahzaGFyZF9pZM4NO2QpomtyqDM5ZmM4MGI4onBkAA.Vl0DZXz5fVFKsrZkktktb-7pqW4Xm0eubcQfgWtM_HY"
}
```
{{</tab>}}
{{</tabs>}}

If you're having issues, [file an issue](https://gitlab.com/signald/signald/-/issues/new)
